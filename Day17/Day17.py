dataFile = "data.txt"

def LoadDataFromFile(fileName):
	data = []
	lines = []
	with open(fileName) as f:
		line = f.readline()
		strData = line.strip().split(',')
		data = [int(x) for x in strData]
	return data

class Computer:

	def __init__(self, code):
		self.code = [x for x in code] + [0]*5000
		self.index = 0
		self.relativeBase = 0

	def GetValue(self, index, paramIndex, params):
		value = self.code[index + paramIndex]
		param = params[paramIndex-1]
		if param == 0:
			return self.code[value]
		elif param == 1:
			return value
		elif param == 2:
			return self.code[self.relativeBase + value]

	def SetValue(self, index, paramIndex, params, setValue):
		value = self.code[index + paramIndex]
		param = params[paramIndex-1]
		if param == 0:
			self.code[value] = setValue
		elif param == 2:
			self.code[self.relativeBase + value] = setValue
		else:
			print("error")

	def ProcessCode(self):
		while self.code[self.index] != 99:
			value = self.code[self.index]
			operation = value % (100)
			params = [((value//100)//(10**i)%10) for i in range(5)]

			if operation == 1:
				source1 = self.GetValue(self.index, 1, params)
				source2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, source1 + source2)
				self.index += 4
			elif operation == 2:
				source1 = self.GetValue(self.index, 1, params)
				source2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, source1 * source2)
				self.index += 4

			elif operation == 3:
				self.SetValue(self.index, 1, params, self.GetInput())
				self.index += 2
			elif operation == 4:
				target = self.GetValue(self.index, 1, params)
				self.GiveOutput(target)
				self.index += 2

			elif operation == 5:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				if param1 != 0:
					self.index = param2
				else:
					self.index += 3
			elif operation == 6:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				if param1 == 0:
					self.index = param2
				else:
					self.index += 3

			elif operation == 7:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, 1 if param1 < param2 else 0)
				self.index += 4
			elif operation == 8:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, 1 if param1 == param2 else 0)
				self.index += 4

			elif operation == 9:
				self.relativeBase += self.GetValue(self.index, 1, params)
				self.index += 2

	def GetInput(self):
		return None

	def GiveOutput(self, value):
		return


class Robot(Computer):

	def __init__(self, code):
		super().__init__(code)
		self.fields = []
		self.line = []
		self.robot = []

	def Print(self):
		for y in range(len(self.fields)):
			for x in range(len(self.fields[y])):
				field = self.fields[y][x]
				if x == self.robot[1] and y == self.robot[0]:
					field = self.robot[2]
				print(field, end='')
			print("")
		print("\n")

	def GetInput(self):
		return

	def GiveOutput(self, value):
		if value == 10:
			self.fields.append(self.line)
			self.line = []
		else:
			field = chr(value)
			if field in ['>', '<', 'V', '^']:
				self.robot = [len(self.fields), len(self.line), field]
				field = '#'
			self.line.append(field)
	
	def IsField(self, x, y, field):
		if y < 0 or y >= len(self.fields):
			return False
		if x < 0 or x >= len(self.fields[y]):
			return False
		return self.fields[y][x] == field

	def IsIntersection(self, x, y):
		if not self.IsField(x, y, '#'):
			return False
		if not self.IsField(x+1, y, '#'):
			return False
		if not self.IsField(x-1, y, '#'):
			return False
		if not self.IsField(x, y+1, '#'):
			return False
		if not self.IsField(x, y-1, '#'):
			return False
		return True

	def CalcIntersections(self):
		self.Print()
		sum = 0
		for y in range(1, len(self.fields)-1):
			for x in range(1, len(self.fields[y])-1):
				if self.IsIntersection(x, y):
					print("{:2}:{:2}".format(x, y))
					sum += x*y
		return sum

program = LoadDataFromFile(dataFile)
droid = Robot(program)
droid.ProcessCode()
print(droid.CalcIntersections())