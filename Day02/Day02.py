
dataFile = "data.txt"

def LoadDataFromFile(fileName):
	data = []
	lines = []
	with open(fileName) as f:
		lines = f.readlines()
	for line in lines:
		data.append(int(line))
	return data

def ProcessCode(code):
	index = 0
	while (index < len(code) and code[index] != 99):		
		#print("{} - {}".format(index, code))
		value = code[index]
		if value == 1:
			index += 1
			source1 = code[index]
			index += 1
			source2 = code[index]
			index += 1
			code[code[index]] = code[source1] + code[source2]
			index += 1
		elif value == 2:
			index += 1
			source1 = code[index]
			index += 1
			source2 = code[index]
			index += 1
			code[code[index]] = code[source1] * code[source2]
			index += 1


code = [1,1,1,4,99,5,6,0,99]

code = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,2,9,19,23,1,9,23,27,2,27,9,31,1,31,5,35,2,35,9,39,1,39,10,43,2,43,13,47,1,47,6,51,2,51,10,55,1,9,55,59,2,6,59,63,1,63,6,67,1,67,10,71,1,71,10,75,2,9,75,79,1,5,79,83,2,9,83,87,1,87,9,91,2,91,13,95,1,95,9,99,1,99,6,103,2,103,6,107,1,107,5,111,1,13,111,115,2,115,6,119,1,119,5,123,1,2,123,127,1,6,127,0,99,2,14,0,0]
#code[1] = 12
#code[2] = 2

for verb in range(0, 99):
	for noun in range(0, 99):
		test = code[:]
		test[1] = noun
		test[2] = verb
		ProcessCode(test)
		if test[0] == 19690720:
			print(noun)
			print(verb)
			print(100*noun + verb)
			break
