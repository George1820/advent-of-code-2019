

dataFile = "data.txt"

def LoadDataFromFile(fileName):
	data = []
	lines = []
	with open(fileName) as f:
		lines = f.readlines()
	for line in lines:
		data.append(line.strip().split(')'))
	return data

class Planet():

	def __init__(self, name):
		self.name = name
		self.orbits = None
		self.childs = []

	def Setup(self, orbits):
		self.orbits = orbits
		if orbits:
			orbits.childs.append(self)

	def Print(self):
		print("{}:".format(self.name))
		for child in self.childs:
			print(" {}".format(child.name))


def CalcOrbits(data):
	tree = {}
	for item in data:
		orbitsName = item[0]
		orbit = tree.get(orbitsName, Planet(orbitsName))
		tree[orbitsName] = orbit
		planetName = item[1]
		planet = tree.get(planetName, Planet(planetName))
		planet.Setup(orbit)
		tree[planetName] = planet
	return tree

def FindPath(start, end):
	queue = [(start, 0)]
	visited = set([start])

	while len(queue) > 0:
		item, step = queue.pop(0)

		if item.orbits:
			if item.orbits == end:
				return step + 1
			elif item.orbits not in visited:
				queue.append((item.orbits, step + 1))
				visited.add(item.orbits)

		for child in item.childs:
			if child == end:
				return step + 1
			elif child not in visited:
				queue.append((child, step + 1))
				visited.add(child)
	return -1
	
data = LoadDataFromFile(dataFile)
tree = CalcOrbits(data)

print(FindPath(tree["YOU"].orbits, tree["SAN"].orbits))