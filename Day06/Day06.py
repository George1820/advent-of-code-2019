
dataFile = "data.txt"

def LoadDataFromFile(fileName):
	data = []
	lines = []
	with open(fileName) as f:
		lines = f.readlines()
	for line in lines:
		data.append(line.strip().split(')'))
	return data

class Planet():

	def __init__(self, name):
		self.name = name
		self.orbits = None
		self.childs = []

	def Setup(self, orbits):
		self.orbits = orbits
		if orbits:
			orbits.childs.append(self)

	def GetOrbits(self):
		orbit = self.orbits
		count = 0
		while orbit:
			count += 1
			orbit = orbit.orbits
		return count

	def Print(self):
		print("{}:".format(self.name))
		for child in self.childs:
			print(" {}".format(child.name))


def CalcOrbits(data):
	tree = {}
	for item in data:
		orbitsName = item[0]
		orbit = tree.get(orbitsName, Planet(orbitsName))
		tree[orbitsName] = orbit
		planetName = item[1]
		planet = tree.get(planetName, Planet(planetName))
		planet.Setup(orbit)
		tree[planetName] = planet
	return tree

data = LoadDataFromFile(dataFile)
tree = CalcOrbits(data)
totalInOrbits = 0
for key, value in tree.items():
	totalInOrbits += max(value.GetOrbits(), 0)

print("Indirect orbits: {}".format(totalInOrbits))
