


def IsValid1(value):
	digits = [int(x) for x in str(value)]
	sameDigits = False
	for i in range(1, len(digits)):
		if digits[i-1] > digits[i]:
			return False
		if digits[i-1] == digits[i]:
			sameDigits = True
	if sameDigits:
		return True
	return False

def IsValid2(value):
	digits = [int(x) for x in str(value)]
	sameDigits = {}
	for i in range(1, len(digits)):
		if digits[i-1] > digits[i]:
			return False

	for i in range(1, len(digits)):
		if digits[i-1] == digits[i]:
			if not sameDigits.get(digits[i]):
				sameDigits[digits[i]] = 2
			else:
				sameDigits[digits[i]] += 1

	anyTwo = False
	for key,value in sameDigits.items():
		anyTwo |= value == 2
	return anyTwo


start = 193651
end = 649729

counter = 0
for i in range(start, end):
	if IsValid2(i):
		counter += 1

print(counter)