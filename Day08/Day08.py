

dataFile = "data.txt"

def LoadDataFromFile(fileName):
	with open(fileName) as f:
		line = f.readline()
		return [int(x) for x in line]

def CreateLayers(data, width, height):
	outData = []
	layerIndex = 0
	while layerIndex < len(data):
		picture = []
		for y in range(height):
			picture.append([(data[layerIndex + (y*width) + x]) for x in range(width)])
		outData.append(picture)
		layerIndex += width*height
	return outData

def CountDigits(layer, digit):
	count = 0
	for line in layer:
		for pixel in line:
			count = count + 1 if pixel == digit else count
	return count

def FindMinLayer(data):
	minZeroes = 9999999
	outData = []
	for i in range(len(data)):
		zeroes = CountDigits(data[i], 0)
		print("layer {} - {}".format(i, zeroes))
		if zeroes < minZeroes:
			minLayer = i
			minZeroes = zeroes
			outData = [CountDigits(data[i], 1), CountDigits(data[i], 2)]
	return minLayer, outData

data = LoadDataFromFile(dataFile)
data = CreateLayers(data, 25, 6)

minLayer, counts = FindMinLayer(data)

print(minLayer)
print(counts)
print(counts[0] * counts[1])