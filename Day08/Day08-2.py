
#dataFile = "data2.txt"
#pictureSize = [2, 2]

dataFile = "data.txt"
pictureSize = [25, 6]


def LoadDataFromFile(fileName):
	with open(fileName) as f:
		line = f.readline()
		return [int(x) for x in line]

def CreateLayers(data, width, height):
	outData = []
	layerIndex = 0
	while layerIndex < len(data):
		picture = []
		for y in range(height):
			picture.append([(data[layerIndex + (y*width) + x]) for x in range(width)])
		outData.append(picture)
		layerIndex += width*height
	return outData

def ComposePicture(data):
	picture = []
	for y in range(len(data[0])):
		line = []
		for x in range(len(data[0][0])):
			for layer in data:
				pixel = layer[y][x]
				if pixel == 2:
					continue
				line.append(pixel)
				break
		picture.append(line)
	return picture
	
def PrintPicture(picture):
	for line in picture:
		for pixel in line:
			if pixel == 0:
				print("  ", end='')
			elif pixel == 1:
				print("XX", end='')
			else:
				print("..", end='')
		print("\n")

data = LoadDataFromFile(dataFile)
data = CreateLayers(data, pictureSize[0], pictureSize[1])

picture = ComposePicture(data)
PrintPicture(picture)