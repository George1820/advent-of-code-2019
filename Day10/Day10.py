
def LoadMap(fileName):
	with open(fileName) as f:
		lines = f.readlines()
	data = []
	for line in lines:
		data.append([x for x in (line.strip())])
	return data


def CalcStep(coord):
	if coord[0] == 0:
		return (0, coord[1]//abs(coord[1]))
	if coord[1] == 0:
		return (coord[0]//abs(coord[0]), 0)
	toInt = min(abs(coord[0]), abs(coord[1]))
	for i in range(toInt, 1, -1):
		if coord[0] % i == 0 and coord[1]%i == 0:
			return (coord[0]//i, coord[1]//i)
	return coord

def IsVisible(data, coord1, coord2):	
	if coord1 == coord2:
		return False
	diff = (coord2[0]-coord1[0], coord2[1]-coord1[1])
	step = CalcStep(diff)
	checkPos = [x for x in coord1]
	while True:
		checkPos[0] += step[0]
		checkPos[1] += step[1]
		if checkPos[0] == coord2[0] and checkPos[1] == coord2[1]:
			return True
		if data[checkPos[1]][checkPos[0]] != '.':
			return False
	print("error")

def GetMeteors(data):
	meteors = {}
	for y in range(len(data)):
		for x in range(len(data[0])):
			if data[y][x] != '.':
				meteors[(x, y)] = 0
	return meteors

def MaxVisibility(data, meteors):
	for fromCoord, fromCount in meteors.items():
		for toCoord, toCount in meteors.items():
			if IsVisible(data, fromCoord, toCoord):
				meteors[fromCoord] += 1

def DrawVisibility(data, meteors, coord):
	for y in range(len(data)):
		for x in range(len(data[0])):
			meteor = meteors.get((x, y))
			if meteor and data[y][x] == '.':
				print("err", end='')
			elif meteor:
				if coord[0] == x and coord[1] == y:
					print(" # ", end='')
				elif IsVisible(data, coord, (x, y)):
					print(" O ", end='')
				else:
					print(" X ", end='')
			else:
				print(" . ", end='')
		print("\n")

def DrawResults(data, meteors):
	for y in range(len(data)):
		for x in range(len(data[0])):
			meteor = meteors.get((x, y))
			if meteor:
				print("{:3}".format(meteor), end='')
			else:
				print(" . ", end='')
		print("\n")

data = LoadMap("data.txt")
meteors = GetMeteors(data)
MaxVisibility(data, meteors)

maxCount = 0
maxCoord = None
total = 0
for coord, count in meteors.items():
	print("{} = {}".format(coord, count))
	total += 1
	if maxCount < count:
		maxCount = count
		maxCoord = coord
print("\nTotal: {}".format(total))
print("Max: {} = {}\n".format(maxCoord, maxCount))

DrawVisibility(data, meteors, maxCoord)
print("\n")
#DrawVisibility(data, meteors, (0,0))


