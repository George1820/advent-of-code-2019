import math

def LoadMap(fileName):
	with open(fileName) as f:
		lines = f.readlines()
	data = []
	for line in lines:
		data.append([x for x in (line.strip())])
	return data

def CalcStep(coord):
	if coord[0] == 0:
		return (0, coord[1]//abs(coord[1]))
	if coord[1] == 0:
		return (coord[0]//abs(coord[0]), 0)
	toInt = min(abs(coord[0]), abs(coord[1]))
	for i in range(toInt, 1, -1):
		if coord[0] % i == 0 and coord[1]%i == 0:
			return (coord[0]//i, coord[1]//i)
	return coord

def IsVisible(meteors, coord1, coord2):	
	if coord1 == coord2:
		return False
	diff = (coord2[0]-coord1[0], coord2[1]-coord1[1])
	step = CalcStep(diff)
	checkPos = [x for x in coord1]
	while True:
		checkPos[0] += step[0]
		checkPos[1] += step[1]
		if checkPos[0] == coord2[0] and checkPos[1] == coord2[1]:
			return True
		if meteors.get( (checkPos[0],checkPos[1]) ) != None:
			return False
	print("error")

def GetMeteors(data):
	meteors = {}
	for y in range(len(data)):
		for x in range(len(data[0])):
			if data[y][x] != '.':
				meteors[(x, y)] = 0
	return meteors

def FindVisible(meteors, coord):
	out = []
	for meteor, fromCount in meteors.items():
		if IsVisible(meteors, coord, meteor):
			out.append(meteor)
	return out

def DrawVisibility(width, height, meteors, coord):
	for y in range(height):
		for x in range(width):
			if x == coord[0] and y == coord[1]:
				print(" # ", end='')
				continue
			if meteors.get((x, y)) != None:
				if IsVisible(meteors, coord, (x, y)):
					print(" X ", end='')
				else:
					print(" O ", end='')
			else:
				print(" . ", end='')
		print("\n")
	print("\n")

def SortMeteors(meteors, station):
	out = []
	for coord in meteors:
		angle = math.atan2( (coord[0] - station[0]), -(coord[1] - station[1]))
		angle += (2*3.1415) if angle < 0 else 0
		out.append((coord, angle))
	out.sort(key=lambda meteor: meteor[1])
	return out

#station = (8, 2)
#data = LoadMap("dataTestLaser1.txt")

#station = (11, 13)
#data = LoadMap("dataTestLaser2.txt")

#station = (8, 3)
#data = LoadMap("dataTestLaser.txt")

station = (23, 19)
data = LoadMap("data.txt")

meteors = GetMeteors(data)
removed = []
while len(meteors) > 1:
	DrawVisibility(len(data[0]), len(data), meteors, station)
	visible = FindVisible(meteors, station)
	removed += SortMeteors(visible, station)
	for meteor in visible:
		del meteors[meteor]

for i in range(len(removed)):
	print("{} - {}".format(i+1, removed[i][0]))