
import math

dataFile = "data.txt"

def Split(item):
	pair = item.strip().split(" ")
	return [int(pair[0]), pair[1].strip()]

def LoadDataFromFile(fileName):
	data = {}
	lines = []
	with open(fileName) as f:
		lines = f.readlines()
	for line in lines:
		parts = line.strip().split("=>")
		ingrediences = [Split(x) for x in parts[0].split(",")]
		product = Split(parts[1])
		data[product[1]] = [product[0], ingrediences, 0]
	return data

def CalcLevel(data):
	distances = {"ORE" : 0}
	while len(distances) < len(data):
		for name, item in data.items():
			if name in distances:
				continue
			allReady = True
			maxDist = 0
			for i in item[1]:
				if not i[1] in distances:
					allReady = False
					break
				maxDist = max(maxDist, distances[i[1]])
			if allReady:
				distances[name] = maxDist + 1
				data[name][2] = maxDist + 1

def GetOreNeeded(fuelCount, data):
	list = [[fuelCount, "FUEL", data["FUEL"][2]]]
	oreNeeded = 0
	while len(list) > 0:
		list.sort(key = lambda x: x[2], reverse=True)

		item = list.pop(0)

		name = item[1]
		itemCount = item[0]
	   
		batches = math.ceil(itemCount / data[name][0])
		for i in data[name][1]:
			found = False
			for j in range(len(list)):
				if list[j][1] == i[1]:
					list[j][0] += batches*i[0]
					found = True
					break
			if not found:
				if i[1] == "ORE":
					oreNeeded += batches*i[0]
				else:
					list.append([batches*i[0], i[1], data[i[1]][2]])
	return oreNeeded


data = LoadDataFromFile(dataFile)
CalcLevel(data)

#print(GetOreNeeded(82892753, data))

targetOre = 1000000000000
oneOreNeeded = GetOreNeeded(1, data)
oreNeeded = oneOreNeeded
fuelProduced = 0
while True:
	step = (targetOre - oreNeeded) // oneOreNeeded
	if step == 0:
		print(fuelProduced)
		break
	fuelProduced += step
	oreNeeded = GetOreNeeded(fuelProduced, data)
