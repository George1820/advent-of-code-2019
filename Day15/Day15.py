dataFile = "data.txt"

def LoadDataFromFile(fileName):
	data = []
	lines = []
	with open(fileName) as f:
		line = f.readline()
		strData = line.strip().split(',')
		data = [int(x) for x in strData]
	return data

class Computer:

	def __init__(self, code):
		self.code = [x for x in code] + [0]*500
		self.index = 0
		self.relativeBase = 0

	def GetValue(self, index, paramIndex, params):
		value = self.code[index + paramIndex]
		param = params[paramIndex-1]
		if param == 0:
			return self.code[value]
		elif param == 1:
			return value
		elif param == 2:
			return self.code[self.relativeBase + value]

	def SetValue(self, index, paramIndex, params, setValue):
		value = self.code[index + paramIndex]
		param = params[paramIndex-1]
		if param == 0:
			self.code[value] = setValue
		elif param == 2:
			self.code[self.relativeBase + value] = setValue
		else:
			print("error")

	def ProcessCode(self):
		while self.code[self.index] != 99:
			value = self.code[self.index]
			operation = value % (100)
			params = [((value//100)//(10**i)%10) for i in range(5)]

			if operation == 1:
				source1 = self.GetValue(self.index, 1, params)
				source2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, source1 + source2)
				self.index += 4
			elif operation == 2:
				source1 = self.GetValue(self.index, 1, params)
				source2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, source1 * source2)
				self.index += 4

			elif operation == 3:
				self.SetValue(self.index, 1, params, self.GetInput())
				self.index += 2
			elif operation == 4:
				target = self.GetValue(self.index, 1, params)
				self.GiveOutput(target)
				self.index += 2

			elif operation == 5:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				if param1 != 0:
					self.index = param2
				else:
					self.index += 3
			elif operation == 6:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				if param1 == 0:
					self.index = param2
				else:
					self.index += 3

			elif operation == 7:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, 1 if param1 < param2 else 0)
				self.index += 4
			elif operation == 8:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, 1 if param1 == param2 else 0)
				self.index += 4

			elif operation == 9:
				self.relativeBase += self.GetValue(self.index, 1, params)
				self.index += 2

	def GetInput(self):
		return None

	def GiveOutput(self, value):
		return


class Droid(Computer):

	def __init__(self, code):
		super().__init__(code)
		self.map = {(0,0):1}
		self.backMap = {1:2, 2:1, 3:4, 4:3}
		self.position = (0,0)
		self.steps = []
		self.min = [-5,-5]
		self.max = [5,5]
		self.lastPos = (0,0)
		self.moves = []

	def Print(self):
		for y in range(self.min[1], self.max[1]+1):
			for x in range(self.min[0], self.max[0]+1):
				if (x, y) == self.position:
					print("X", end='')
					continue
				value = self.map.get((x, y), -1)
				if value == 0:
					print("#", end='')
				elif value == 1:
					print(".", end='')
				elif value == 2:
					print("O", end='')
				else:
					print(" ", end='')
			print("")
		print("\n")

	def GenerateSteps(self):
		out = []
		if not (self.position[0], self.position[1]-1) in self.map:
			out.append(1)
		if not (self.position[0], self.position[1]+1) in self.map:
			out.append(2)
		if not (self.position[0]-1, self.position[1]) in self.map:
			out.append(3)
		if not (self.position[0]+1, self.position[1]) in self.map:
			out.append(4)
		return out

	def MakeMove(self, move):
		self.lastPos = self.position
		if move == 1:
			self.position = (self.position[0], self.position[1]-1)
		elif move == 2:
			self.position = (self.position[0], self.position[1]+1)
		elif move == 3:
			self.position = (self.position[0]-1, self.position[1])
		elif move == 4:
			self.position = (self.position[0]+1, self.position[1])
		self.min[0] = min(self.min[0], self.position[0])
		self.max[0] = max(self.max[0], self.position[0])
		self.min[1] = min(self.min[1], self.position[1])
		self.max[1] = max(self.max[1], self.position[1])
		return move

	def GetInput(self):
		steps = self.GenerateSteps()
		if len(steps) > 0:
			self.moves.append(steps[0])
			return self.MakeMove(steps[0])
		if len(self.moves) > 0:
			lastMove = self.moves.pop(-1)
			return self.MakeMove(self.backMap[lastMove])
		self.Print()
		self.CalculatePath()
		return 1

	def GiveOutput(self, value):
		self.map[self.position] = value
		if value == 0:
			self.position = self.lastPos
			self.moves.pop(-1)
		#self.Print()

	def IsValidPos(self, position, visited):
		if not position in self.map:
			return False
		if position in visited:
			return False
		if self.map[position] == 0:
			return False
		return True

	def NextPositions(self, position, visited):
		out = []
		newPos = (position[0], position[1]-1)
		if self.IsValidPos(newPos, visited):
			out.append(newPos)
		newPos = (position[0], position[1]+1)
		if self.IsValidPos(newPos, visited):
			out.append(newPos)
		newPos = (position[0]-1, position[1])
		if self.IsValidPos(newPos, visited):
			out.append(newPos)
		newPos = (position[0]+1, position[1])
		if self.IsValidPos(newPos, visited):
			out.append(newPos)
		return out

	def CalculatePath(self):
		visited = set()
		visited.add(self.position)
		queue = [[self.position, 0]]
		while len(queue) > 0:
			item = queue.pop(0)
			nextPositions = self.NextPositions(item[0], visited)
			for i in nextPositions:
				if self.map[i] == 2:
					print("Steps: {}".format(item[1] + 1))
				else:
					queue.append([i, item[1]+1])
					visited.add(i)

program = LoadDataFromFile(dataFile)
droid = Droid(program)
droid.ProcessCode()