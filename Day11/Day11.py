class Computer:

	def __init__(self, code):
		self.code = [x for x in code] + [0]*500
		self.index = 0
		self.relativeBase = 0

	def GetValue(self, index, paramIndex, params):
		value = self.code[index + paramIndex]
		param = params[paramIndex-1]
		if param == 0:
			return self.code[value]
		elif param == 1:
			return value
		elif param == 2:
			return self.code[self.relativeBase + value]

	def SetValue(self, index, paramIndex, params, setValue):
		value = self.code[index + paramIndex]
		param = params[paramIndex-1]
		if param == 0:
			self.code[value] = setValue
		elif param == 2:
			self.code[self.relativeBase + value] = setValue
		else:
			print("error")

	def ProcessCode(self):
		while self.code[self.index] != 99:
			value = self.code[self.index]
			operation = value % (100)
			params = [((value//100)//(10**i)%10) for i in range(5)]

			if operation == 1:
				source1 = self.GetValue(self.index, 1, params)
				source2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, source1 + source2)
				self.index += 4
			elif operation == 2:
				source1 = self.GetValue(self.index, 1, params)
				source2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, source1 * source2)
				self.index += 4

			elif operation == 3:
				self.SetValue(self.index, 1, params, self.GetInput())
				self.index += 2
			elif operation == 4:
				target = self.GetValue(self.index, 1, params)
				self.GiveOutput(target)
				self.index += 2

			elif operation == 5:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				if param1 != 0:
					self.index = param2
				else:
					self.index += 3
			elif operation == 6:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				if param1 == 0:
					self.index = param2
				else:
					self.index += 3

			elif operation == 7:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, 1 if param1 < param2 else 0)
				self.index += 4
			elif operation == 8:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, 1 if param1 == param2 else 0)
				self.index += 4

			elif operation == 9:
				self.relativeBase += self.GetValue(self.index, 1, params)
				self.index += 2

	def GetInput(self):
		return None

	def GiveOutput(self, value):
		return


class Robot(Computer):

	def __init__(self, code):
		super().__init__(code)
		self.direction = 0
		self.position = (0, 0)
		self.tiles = {}
		self.min = [-3,-3]
		self.max = [ 3, 3]
		self.commands = []

	def Paint(self, coord, color):
		self.min[0] = min(coord[0], self.min[0])
		self.min[1] = min(coord[1], self.min[1])
		self.max[0] = max(coord[0], self.max[0])
		self.max[1] = max(coord[1], self.max[1])
		self.tiles[coord] = color

	def Print(self):
		for y in range(self.min[1], self.max[1]+1):
			for x in range(self.min[0], self.max[0]+1):
				if self.position == (x, y):
					if self.direction == 0:
						print('^', end='')
					elif self.direction == 1:
						print('>', end='')
					elif self.direction == 2:
						print('v', end='')
					elif self.direction == 3:
						print('<', end='')
				else:
					value = self.tiles.get((x, y), 0)
					if value == 0:
						print('.', end='')
					else:
						print('#', end='')
			print("\n")
		print("\n")

	def TilesNum(self):
		return len(self.tiles)

	def MakeStep(self):
		if self.direction == 0:
			self.position = (self.position[0], self.position[1]-1)
		elif self.direction == 1:
			self.position = (self.position[0]+1, self.position[1])
		elif self.direction == 2:
			self.position = (self.position[0], self.position[1]+1)
		elif self.direction == 3:
			self.position = (self.position[0]-1, self.position[1])

	def MakeTurn(self, direction):
		self.direction = self.direction + 1 if direction == 1 else self.direction - 1
		self.direction = (self.direction + 4)%4

	def GetInput(self):
		return self.tiles.get(self.position, 0)
	
	def GiveOutput(self, value):
		self.commands.append(value)
		if len(self.commands) < 2:
			return
		self.Paint(self.position, self.commands[0])
		self.MakeTurn(self.commands[1])
		self.MakeStep()
		#print(self.commands)
		#self.Print()
		self.commands = []

code = [3,8,1005,8,299,1106,0,11,0,0,0,104,1,104,0,3,8,102,-1,8,10,101,1,10,10,4,10,108,1,8,10,4,10,102,1,8,28,1006,0,85,1,106,14,10,3,8,102,-1,8,10,1001,10,1,10,4,10,1008,8,0,10,4,10,101,0,8,58,1,1109,15,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,0,10,4,10,1002,8,1,84,3,8,102,-1,8,10,1001,10,1,10,4,10,108,1,8,10,4,10,1002,8,1,105,1006,0,48,3,8,1002,8,-1,10,1001,10,1,10,4,10,108,0,8,10,4,10,102,1,8,130,1006,0,46,1,1001,17,10,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,0,10,4,10,1002,8,1,160,2,109,20,10,3,8,102,-1,8,10,1001,10,1,10,4,10,108,0,8,10,4,10,1002,8,1,185,3,8,102,-1,8,10,1001,10,1,10,4,10,108,1,8,10,4,10,1001,8,0,207,1006,0,89,2,1002,6,10,1,1007,0,10,3,8,102,-1,8,10,101,1,10,10,4,10,1008,8,1,10,4,10,101,0,8,241,2,4,14,10,3,8,102,-1,8,10,101,1,10,10,4,10,1008,8,1,10,4,10,101,0,8,267,1,1107,8,10,1,109,16,10,2,1107,4,10,101,1,9,9,1007,9,1003,10,1005,10,15,99,109,621,104,0,104,1,21101,0,387239486208,1,21102,316,1,0,1106,0,420,21101,0,936994976664,1,21102,327,1,0,1105,1,420,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,21102,1,29192457307,1,21102,1,374,0,1106,0,420,21101,0,3450965211,1,21101,0,385,0,1106,0,420,3,10,104,0,104,0,3,10,104,0,104,0,21102,1,837901103972,1,21101,408,0,0,1106,0,420,21102,867965752164,1,1,21101,0,419,0,1105,1,420,99,109,2,22102,1,-1,1,21102,40,1,2,21102,451,1,3,21102,1,441,0,1106,0,484,109,-2,2106,0,0,0,1,0,0,1,109,2,3,10,204,-1,1001,446,447,462,4,0,1001,446,1,446,108,4,446,10,1006,10,478,1102,0,1,446,109,-2,2105,1,0,0,109,4,1201,-1,0,483,1207,-3,0,10,1006,10,501,21101,0,0,-3,22101,0,-3,1,22102,1,-2,2,21101,1,0,3,21101,520,0,0,1106,0,525,109,-4,2106,0,0,109,5,1207,-3,1,10,1006,10,548,2207,-4,-2,10,1006,10,548,21201,-4,0,-4,1105,1,616,22101,0,-4,1,21201,-3,-1,2,21202,-2,2,3,21101,0,567,0,1106,0,525,22101,0,1,-4,21101,1,0,-1,2207,-4,-2,10,1006,10,586,21102,1,0,-1,22202,-2,-1,-2,2107,0,-3,10,1006,10,608,21202,-1,1,1,21102,608,1,0,106,0,483,21202,-2,-1,-2,22201,-4,-2,-4,109,-5,2105,1,0]

robot = Robot(code)
robot.ProcessCode()
print("Number of tiles: {}".format(robot.TilesNum()))
