
dataFile = "data.txt"

def LoadDataFromFile(fileName):
	data = []
	lines = []
	with open(fileName) as f:
		line = f.readline()
		strData = line.strip().split(',')
		data = [int(x) for x in strData]
	return data

class Computer:

	def __init__(self, code):
		self.code = [x for x in code] + [0]*500
		self.index = 0
		self.relativeBase = 0

	def GetValue(self, index, paramIndex, params):
		value = self.code[index + paramIndex]
		param = params[paramIndex-1]
		if param == 0:
			return self.code[value]
		elif param == 1:
			return value
		elif param == 2:
			return self.code[self.relativeBase + value]

	def SetValue(self, index, paramIndex, params, setValue):
		value = self.code[index + paramIndex]
		param = params[paramIndex-1]
		if param == 0:
			self.code[value] = setValue
		elif param == 2:
			self.code[self.relativeBase + value] = setValue
		else:
			print("error")

	def ProcessCode(self):
		while self.code[self.index] != 99:
			value = self.code[self.index]
			operation = value % (100)
			params = [((value//100)//(10**i)%10) for i in range(5)]

			if operation == 1:
				source1 = self.GetValue(self.index, 1, params)
				source2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, source1 + source2)
				self.index += 4
			elif operation == 2:
				source1 = self.GetValue(self.index, 1, params)
				source2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, source1 * source2)
				self.index += 4

			elif operation == 3:
				self.SetValue(self.index, 1, params, self.GetInput())
				self.index += 2
			elif operation == 4:
				target = self.GetValue(self.index, 1, params)
				self.GiveOutput(target)
				self.index += 2

			elif operation == 5:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				if param1 != 0:
					self.index = param2
				else:
					self.index += 3
			elif operation == 6:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				if param1 == 0:
					self.index = param2
				else:
					self.index += 3

			elif operation == 7:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, 1 if param1 < param2 else 0)
				self.index += 4
			elif operation == 8:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				self.SetValue(self.index, 3, params, 1 if param1 == param2 else 0)
				self.index += 4

			elif operation == 9:
				self.relativeBase += self.GetValue(self.index, 1, params)
				self.index += 2

	def GetInput(self):
		return None

	def GiveOutput(self, value):
		return

class Arcade(Computer):
	
	def __init__(self, code):
		super().__init__(code)
		self.board = {}
		self.outputs = []
		self.count = {}
		self.ballPos = None
		self.paddlePos = None
		self.min = [0,0]
		self.max = [0,0]

	def Print(self):
		for y in range(self.min[1], self.max[1]+1):
			for x in range(self.min[0], self.max[0]+1):
				value = self.board.get((x, y), 0)
				if value == 1:
					print("#", end='')
				elif value == 2:
					print("@", end='')
				elif value == 3:
					print("-", end='')
				elif value == 4:
					print("O", end='')
				else:
					print(" ", end='')
			print("")
		print("\n")

	def GetInput(self):
		if self.ballPos[0] > self.paddlePos[0]:
			return 1
		if self.ballPos[0] < self.paddlePos[0]:
			return -1
		return 0

	def GiveOutput(self, value):
		self.outputs.append(value)
		if len(self.outputs) < 3:
			return
		x = self.outputs[0]
		y = self.outputs[1]
		id = self.outputs[2]
		self.outputs = []
		
		if x == -1 and y == 0:
			print("score: {}".format(id))
			return

		if id == 4:
			self.ballPos = (x, y)
		elif id == 3:
			self.paddlePos = (x, y)

		self.count[id] = self.count.get(id, 0) + 1
		self.board[(x, y)] = id
		self.min[0] = min(x, self.min[0])
		self.min[1] = min(y, self.min[1])
		self.max[0] = max(x, self.max[0])
		self.max[1] = max(y, self.max[1])
		#self.Print()

program = LoadDataFromFile(dataFile)
program[0] = 2

arcade = Arcade(program)
arcade.ProcessCode()