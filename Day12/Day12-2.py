from math import gcd

class Moon:

	def __init__(self, position):
		self.startPos = position
		self.position = position
		self.velocity = [0, 0, 0]

	def Print(self):
		print("{} - {}".format(self.position, self.velocity))

	def UpdatePosition(self, index):
		self.position[index] += self.velocity[index]

	def GetSign(self, x):
		if x > 0:
			return 1
		if x < 0:
			return -1
		return 0

	def UpdateVelocity(self, moons, index):
		for moon in moons:
			if moon == self:
				continue
			self.velocity[index] += self.GetSign(moon.position[index] - self.position[index])
		
"""
<x=-7, y=17, z=-11>
<x=9, y=12, z=5>
<x=-9, y=0, z=-4>
<x=4, y=6, z=0>
"""

moons = []
moons.append(Moon([-7, 17, -11]))
moons.append(Moon([9, 12, 5]))
moons.append(Moon([-9, 0, -4]))
moons.append(Moon([4, 6, 0]))


"""
pos=<x= -1, y=  0, z=  2>, vel=<x=  0, y=  0, z=  0>
pos=<x=  2, y=-10, z= -7>, vel=<x=  0, y=  0, z=  0>
pos=<x=  4, y= -8, z=  8>, vel=<x=  0, y=  0, z=  0>
pos=<x=  3, y=  5, z= -1>, vel=<x=  0, y=  0, z=  0>
"""
"""
moons = []
moons.append(Moon([ -1,  0,  2]))
moons.append(Moon([  2,-10, -7]))
moons.append(Moon([  4, -8,  8]))
moons.append(Moon([  3,  5, -1]))
"""

cycles = [None]*3
seen = [set()]*3

for cycle in range(0, 277700000):
	for i in range(3):
		if cycles[i] == None:
			state = str([(moon.position[i], moon.velocity[i]) for moon in moons])
			if state in seen[i]:
				cycles[i] = cycle
			else:
				seen[i].add(state)

	if cycles[0] != None and cycles[1] != None and cycles[2] != None:
		break

	for moon in moons:
		moon.UpdateVelocity(moons, 0)
		moon.UpdateVelocity(moons, 1)
		moon.UpdateVelocity(moons, 2)
	for moon in moons:
		moon.UpdatePosition(0)
		moon.UpdatePosition(1)
		moon.UpdatePosition(2)

lcm = cycles[0]
for i in cycles[1:]:
  lcm = int(lcm*i/gcd(lcm, i))
print(lcm)

