class Moon:

	def __init__(self, position):
		self.position = position
		self.velocity = [0, 0, 0]

	def Print(self):
		print("{} - {}".format(self.position, self.velocity))

	def UpdatePosition(self):
		self.position[0] += self.velocity[0]
		self.position[1] += self.velocity[1]
		self.position[2] += self.velocity[2]

	def GetSign(self, x):
		if x > 0:
			return 1
		if x < 0:
			return -1
		return 0

	def UpdateVelocity(self, moons):
		for moon in moons:
			if moon == self:
				continue
			self.velocity[0] += self.GetSign(moon.position[0] - self.position[0])
			self.velocity[1] += self.GetSign(moon.position[1] - self.position[1])
			self.velocity[2] += self.GetSign(moon.position[2] - self.position[2])
			
	def GetPotencialEnergy(self):
		return abs(self.position[0]) + abs(self.position[1]) + abs(self.position[2])

	def GetKineticEnergy(self):
		return abs(self.velocity[0]) + abs(self.velocity[1]) + abs(self.velocity[2])


"""
<x=-7, y=17, z=-11>
<x=9, y=12, z=5>
<x=-9, y=0, z=-4>
<x=4, y=6, z=0>
"""

moons = []
moons.append(Moon([-7, 17, -11]))
moons.append(Moon([9, 12, 5]))
moons.append(Moon([-9, 0, -4]))
moons.append(Moon([4, 6, 0]))

"""
<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>
"""

"""
moons = []
moons.append(Moon([-8, -10, 0]))
moons.append(Moon([5, 5, 10]))
moons.append(Moon([2, -7, 3]))
moons.append(Moon([9, -8, -3]))
"""

for i in range(1000):
	for moon in moons:
		moon.UpdateVelocity(moons)
	for moon in moons:
		moon.UpdatePosition()
		#moon.Print()
	#print("-----------------------------------------------")

totalEnergy = 0
for moon in moons:
	totalEnergy += moon.GetPotencialEnergy() * moon.GetKineticEnergy()
print("Total energy: {}".format(totalEnergy))