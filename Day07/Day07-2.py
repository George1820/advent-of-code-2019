import itertools

class Computer:
	def __init__(self, setting, code):
		self.code = [x for x in code]
		self.inputs = [setting]
		self.index = 0

	def GetValue(self, index, paramIndex, params):
		value = self.code[index + paramIndex]
		param = params[paramIndex-1]
		if param == 0:
			return self.code[value]
		elif param == 1:
			return value

	def ProcessCode(self, inInputs):
		self.inputs = self.inputs + [x for x in inInputs]
		if len(self.inputs) == 0:
			print("No input!!!")
			return [], True
		outputs = []
		while (self.index < len(self.code) and self.code[self.index] != 99):		
			value = self.code[self.index]
			operation = value % (100)
			params = [((value//100)//(10**i)%10) for i in range(5)]

			if operation == 1:
				source1 = self.GetValue(self.index, 1, params)
				source2 = self.GetValue(self.index, 2, params)
				self.code[self.code[self.index + 3]] = source1 + source2
				self.index += 4
			elif operation == 2:
				source1 = self.GetValue(self.index, 1, params)
				source2 = self.GetValue(self.index, 2, params)
				self.code[self.code[self.index + 3]] = source1 * source2
				self.index += 4

			elif operation == 3:
				if len(self.inputs) == 0:
					return outputs, False
				inValue = self.inputs.pop(0)
				target = self.code[self.index + 1]
				self.index += 2
				self.code[target] = inValue
			elif operation == 4:
				target = self.GetValue(self.index, 1, params)
				outputs.append(target)
				self.index += 2

			elif operation == 5:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				if param1 != 0:
					self.index = param2
				else:
					self.index += 3
			elif operation == 6:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				if param1 == 0:
					self.index = param2
				else:
					self.index += 3

			elif operation == 7:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				self.code[self.code[self.index + 3]] = 1 if param1 < param2 else 0
				self.index += 4
			elif operation == 8:
				param1 = self.GetValue(self.index, 1, params)
				param2 = self.GetValue(self.index, 2, params)
				self.code[self.code[self.index + 3]] = 1 if param1 == param2 else 0
				self.index += 4

		return outputs, True

def CalculateOutput(program, settings):
	computers = [Computer(x, program) for x in settings]
	data = [0]
	ended = False
	while not ended:
		for computer in computers:
			data, ended = computer.ProcessCode(data)
	return data[0]

def FindMaxSettings(program):
	permutations = list(itertools.permutations([5,6,7,8,9]))
	maxOutput = 0
	for permutation in permutations:
		output = CalculateOutput(program, permutation)
		maxOutput = max(output, maxOutput)
	return maxOutput;

program = [3,8,1001,8,10,8,105,1,0,0,21,38,55,64,81,106,187,268,349,430,99999,3,9,101,2,9,9,1002,9,2,9,101,5,9,9,4,9,99,3,9,102,2,9,9,101,3,9,9,1002,9,4,9,4,9,99,3,9,102,2,9,9,4,9,99,3,9,1002,9,5,9,1001,9,4,9,102,4,9,9,4,9,99,3,9,102,2,9,9,1001,9,5,9,102,3,9,9,1001,9,4,9,102,5,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,99]
print(FindMaxSettings(program))