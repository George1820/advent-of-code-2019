import itertools

class Computer:

	def GetDigits(self, value, start, count):
		return value // 10**start % (10**count)

	def GetValue(self, code, index, paramIndex, params):
		value = code[index + paramIndex]
		param = params[paramIndex-1]
		if param == 0:
			return code[value]
		elif param == 1:
			return value

	def ProcessCode(self, inCode, inInputs):
		code = [x for x in inCode]
		inputs = [x for x in inInputs]
		index = 0
		while (index < len(code) and code[index] != 99):		
			#print("{} - {}".format(index, code))
			value = code[index]
			operation = self.GetDigits(value, 0, 2)
			params = [((value//100)//(10**i)%10) for i in range(5)]

			if operation == 1:
				source1 = self.GetValue(code, index, 1, params)
				source2 = self.GetValue(code, index, 2, params)
				code[code[index + 3]] = source1 + source2
				index += 4
			elif operation == 2:
				source1 = self.GetValue(code, index, 1, params)
				source2 = self.GetValue(code, index, 2, params)
				code[code[index + 3]] = source1 * source2
				index += 4
			elif operation == 3:
				#inValue = int(input("enter value: "))
				inValue = inputs.pop(0)
				target = code[index + 1]
				index += 2
				code[target] = inValue
			elif operation == 4:
				target = self.GetValue(code, index, 1, params)
				return target
				#index += 2

			elif operation == 5:
				param1 = self.GetValue(code, index, 1, params)
				param2 = self.GetValue(code, index, 2, params)
				if param1 != 0:
					index = param2
				else:
					index += 3
			elif operation == 6:
				param1 = self.GetValue(code, index, 1, params)
				param2 = self.GetValue(code, index, 2, params)
				if param1 == 0:
					index = param2
				else:
					index += 3

			elif operation == 7:
				param1 = self.GetValue(code, index, 1, params)
				param2 = self.GetValue(code, index, 2, params)
				code[code[index + 3]] = 1 if param1 < param2 else 0
				index += 4
			elif operation == 8:
				param1 = self.GetValue(code, index, 1, params)
				param2 = self.GetValue(code, index, 2, params)
				code[code[index + 3]] = 1 if param1 == param2 else 0
				index += 4

def CalculateOutput(program, settings):
	moduleInput = 0
	for setting in settings:
		computer = Computer()
		moduleInput = computer.ProcessCode(program, [setting, moduleInput])
	return moduleInput

def FindMaxSettings(program):
	permutations = list(itertools.permutations([0, 1, 2, 3, 4]))
	maxOutput = 0
	for permutation in permutations:
		output = CalculateOutput(program, permutation)
		maxOutput = max(output, maxOutput)
	return maxOutput;


#program = [3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0]
program = [3,8,1001,8,10,8,105,1,0,0,21,38,55,64,81,106,187,268,349,430,99999,3,9,101,2,9,9,1002,9,2,9,101,5,9,9,4,9,99,3,9,102,2,9,9,101,3,9,9,1002,9,4,9,4,9,99,3,9,102,2,9,9,4,9,99,3,9,1002,9,5,9,1001,9,4,9,102,4,9,9,4,9,99,3,9,102,2,9,9,1001,9,5,9,102,3,9,9,1001,9,4,9,102,5,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,99]
output = FindMaxSettings(program)
print(output)